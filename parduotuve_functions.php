<?php

function gauti_duombaze() {
    try {
        $pdo = new PDO('mysql:host=localhost', 'root', ''); #localhost arba 127.0.0.1
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch (PDOException $e) {
        echo $e;
        return 'Error';
    }
}

function isduombazes() {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $prekes = $pdo->prepare('SELECT * FROM prekes;');
    $prekes->execute();
    return $prekes->fetchAll(PDO::FETCH_CLASS);
}

function irasymas($preke, $kaina) {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $kaina = $kaina;
    $uzklausa = $pdo->prepare('INSERT INTO prekes (preke, kaina) VALUES (:preke, :kaina);');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->execute();
}

function istrynimas($id) {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('DELETE FROM prekes WHERE ID=:id');
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    } else {
        return false;
    }
}
 function redagavimas($id, $preke, $kaina) {
    $kaina = $kaina;
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('UPDATE prekes SET preke=:preke, kaina=:kaina WHERE ID=:id');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    }
    return false;
 }